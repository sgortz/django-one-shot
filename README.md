# Django One Shot

Simple todo app that allows for due date and the ability to mark tasks as completed.

## Tech Stack
- [Django](https://www.djangoproject.com/)
- HTML

## Dependencies used
- [pip](https://pypi.org/project/pip/)
- [Black](https://pypi.org/project/black/)
- [Flake8](https://flake8.pycqa.org/en/latest/)
- [Djlint](https://djlint.com/)
