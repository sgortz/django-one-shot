from django.shortcuts import get_object_or_404, render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def show_todos(request):
    context = {
        "todo_lists": TodoList.objects.all(),
    }
    return render(request, "todos/list.html", context)


def show_todo_items(request, pk):
    context = {"todo_list": TodoList.objects.get(pk=pk)}
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST" and TodoListForm:
        form = TodoListForm(request.POST)
        if form.is_valid():
            instance = form.save()
            return redirect("todo_list_detail", pk=instance.pk)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def create_todo_item(request):
    if request.method == "POST" and TodoItemForm:
        form = TodoItemForm(request.POST)
        if form.is_valid():
            instance = form.save()
            todo_list = instance.list
            return redirect("todo_list_detail", pk=todo_list.pk)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def change_todo_list(request, pk):
    if TodoList and TodoListForm:
        instance = TodoList.objects.get(pk=pk)
        if request.method == "POST":
            form = TodoListForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("todo_list_detail", pk=pk)
        else:
            form = TodoListForm(instance=instance)
    else:
        form = None
    context = {"form": form}
    return render(request, "todos/edit.html", context)


def change_todo_item(request, pk):
    if TodoItem and TodoItemForm:
        instance = TodoItem.objects.get(pk=pk)
        if request.method == "POST":
            form = TodoItemForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return redirect("todo_list_detail", pk=pk)
        else:
            form = TodoItemForm(instance=instance)
    else:
        form = None
    context = {"form": form}
    return render(request, "todos/edit_item.html", context)


def delete_todo_list(request, pk):
    context = {}
    obj = get_object_or_404(TodoList, pk=pk)

    if request.method == "POST":
        obj.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html", context)
