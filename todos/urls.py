"""todo_list_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from todos.views import (
    show_todos,
    show_todo_items,
    create_todo_list,
    create_todo_item,
    change_todo_list,
    change_todo_item,
    delete_todo_list,
)

urlpatterns = [
    path("", show_todos, name="todo_list_list"),
    path("<int:pk>/", show_todo_items, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:pk>/edit/", change_todo_list, name="todo_list_update"),
    path("<int:pk>/delete/", delete_todo_list, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("items/<int:pk>/edit/", change_todo_item, name="todo_item_update"),
]
